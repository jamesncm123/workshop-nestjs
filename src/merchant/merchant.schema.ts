import { Schema } from "mongoose";

export const MerchantSchema = new Schema({
    userId: Schema.Types.String,
    fristName: Schema.Types.String,
    lastName: Schema.Types.String,
    age: Schema.Types.Number,
    createdAT: Schema.Types.Date
})