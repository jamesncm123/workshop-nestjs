import { Document } from 'mongoose';

export interface Merchant extends Document{
    userId: string;
    fristName: string;
    lastName: string;
    age: number;
    createdAT: Date;
}