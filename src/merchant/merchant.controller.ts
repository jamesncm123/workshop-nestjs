import { Controller, Get, Post, Param, Body, Res, HttpStatus } from '@nestjs/common';
import { MerchantService } from './merchant.service';

@Controller('api/v1/merchant')
export class MerchantController {
    constructor(private service:MerchantService){}

    @Post('profile')
    async insertOrUpdate(@Body() body, @Res() res){
    const result = await this.service.insertOrUpdate(body);
    return res.status(HttpStatus.OK).json(result);
    }
    @Post('delete/:userId')
    async delete(@Param('userId') userId, @Res() res){
        const result = await this.service.deleteByUserId(userId);
        if(result.status == "success"){
            return await res.status(HttpStatus.OK).json(result);
        }else{
            return await res.status(HttpStatus.BAD_REQUEST).json(result);
        }
    }

}

