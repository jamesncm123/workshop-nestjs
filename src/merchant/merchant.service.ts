import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Merchant } from './merchant.interface';


@Injectable()
export class MerchantService {

    constructor(@InjectModel('Merchant') private readonly modelMerchant: Model<Merchant>){

    }
    
    async insertOrUpdate(body){
        const check = await this.modelMerchant.findOne({userId:body.userId}).exec();
        if(check === null){
            const newMerchant = new this.modelMerchant(body);
            newMerchant.createdAT = new Date();
            await newMerchant.save();
        return newMerchant;
        }else{
            const update = await this.modelMerchant.update(
            {
                userId:body.userId
            },
            {
                fristName:body.fristName,
                lastName:body.lastName,
                age:body.age
            },
            {
                upsert:false
            }).exec();
        
        return update;
        }
    }
    async deleteByUserId(userId){
        const check = await this.modelMerchant.findOne({userId:userId}).exec();
        if(check === null){
        return{
            status: "error"
        }
        }else{
        await this.modelMerchant.remove({userId:userId}).exec();
        return{
            status: "success"
        }
        }
    
}
}
