import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { FeedModule } from './feed/feed.module';
import { MerchantModule } from './merchant/merchant.module';


@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://root:testpassword@cluster0-qkd4e.mongodb.net/user?retryWrites=true&w=majority'),
    UserModule,
    FeedModule,  
    MerchantModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
