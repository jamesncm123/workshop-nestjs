import { Module } from '@nestjs/common';
import { FeedController } from './feed.controller';
import { FeedService } from './feed.service';
import { MongooseModule } from '@nestjs/mongoose';
import { feedSchema } from './feed.schema';

@Module({
  imports:[
        MongooseModule.forFeature([
          {name: "Feed", schema: feedSchema}
        ])
  ],
  controllers: [FeedController],
  providers: [FeedService]
})
export class FeedModule {}
