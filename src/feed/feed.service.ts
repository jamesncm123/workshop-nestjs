import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Feed } from './feed.interface';

@Injectable()
export class FeedService {
    constructor(@InjectModel('Feed') private readonly modelFeed: Model<Feed>){}
    
    async insert(body:any){
        console.log(body)
        const newFeed = new this.modelFeed(body);
        newFeed.createdAt = new Date();
        await newFeed.save();
        return body;
    }

    async getData(){
        const result = await this.modelFeed.find().exec();
        return result;
    }
}
