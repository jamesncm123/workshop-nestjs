import { Controller, Post, Body, Res, HttpStatus } from '@nestjs/common';
import { FeedService } from './feed.service';

@Controller('feed')
export class FeedController {

    constructor(private service:FeedService){}

    @Post('insert')
    async insertFeed(@Body() body, @Res() res){
           const result =  await this.service.insert(body);
           return res.status(HttpStatus.OK).json(result);
        
    }

    @Post('get')
    async getFeed(){
        return await this.service.getData();
    }

}
