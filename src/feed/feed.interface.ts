import { Document } from 'mongoose';

export interface Feed extends Document{
    massage: string;
    urlImage: string;
    createdAt: Date;
}